terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"
        }
    }
}

provider "aws" {
    region = "eu-west-2"
    access_key = var.AWS_ACCESS_KEY != "" ? var.AWS_ACCESS_KEY : null
    secret_key = var.AWS_SECRET_KEY != "" ? var.AWS_ACCESS_KEY : null
    shared_credentials_file = var.AWS_ACCESS_KEY == "" && var.AWS_SECRET_KEY == "" ? var.shared_credentials_file : null
    profile = var.aws_profile
}

resource "aws_s3_bucket" "bucket" {
    bucket = "bmw.health"
    acl = "private"
    policy =  <<EOF
{
"Version": "2012-10-17",
"Statement": [
    {
        "Sid": "PublicRead",
        "Effect": "Allow",
        "Principal": "*",
        "Action": "s3:GetObject",
        "Resource": "arn:aws:s3:::bmw.health/*"
        }
    ]
}
EOF
    
    website {
        index_document = "index.html"
        error_document = "index.html"
    }
    versioning {
        enabled = false
    }
}

resource "null_resource" "uploadscript" {
    provisioner "local-exec" {
        command = "cd ../; ./upload.sh bmw.health"
    }
    depends_on = [aws_s3_bucket.bucket]
}