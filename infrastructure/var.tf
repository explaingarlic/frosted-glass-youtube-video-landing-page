variable "shared_credentials_file" {
    type = string
    default = "~/.aws/credentials"
    description = "AWS [Shared credentails file](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/create-shared-credentials-file.html)."
    sensitive = false # Arguably wrong.
}
variable "aws_profile" {
    type = string
    default = "default"
    description = "AWS [Profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) to use."
    sensitive = false
}
variable "AWS_ACCESS_KEY" {
    type = string
    default = ""
    description = "AWS [Access Key](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html)"
    sensitive = true
}
variable "AWS_SECRET_KEY" {
    type = string
    default = ""
    description = "AWS [Secret Key](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html)"
    sensitive = true
}