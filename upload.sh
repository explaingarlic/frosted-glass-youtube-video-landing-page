#!/bin/bash

if [ -z ${1+x} ]; then 
    SITENAME=$SITENAME
else 
    SITENAME=$1
fi

echo Uploading Site ${SITENAME}
aws s3 sync --delete thesite/ s3://${SITENAME}